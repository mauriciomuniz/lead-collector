package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Lead salvar(CadastroLeadDTO cadastroLeadDTO) {
        Lead lead = cadastroLeadDTO.converterParaLead();

        List<Integer> idDeProdutos = new ArrayList<>();
        for(IdProdutoDTO idProdutoDTO : cadastroLeadDTO.getProdutos()){
            int id = idProdutoDTO.getId();
            idDeProdutos.add(id);
        }
        Iterable<Produto> produtos = produtoRepository.findAllById(idDeProdutos);

        lead.setProduto((List<Produto>) produtos);

        Lead objetoLead = leadRepository.save(lead);
        return objetoLead;

    }

    public Iterable<Lead> lerTodosOsLeads() {
        return leadRepository.findAll();
    }

    public Lead buscarLeadPeloId(int id) {
        Optional<Lead> leadOptional = leadRepository.findById(id);
        if (leadOptional.isPresent()) {
            Lead lead = leadOptional.get();
            return lead;
        } else {
            throw new RuntimeException("O lead nao foi encontrado");
        }
    }

    public Lead atualizarLead(int id, Lead lead) {

        Lead leadDB = buscarLeadPeloId(id);
        lead.setId(leadDB.getId());
        return leadRepository.save(lead);
    }

    public void deletarLead (int id) {
        if (leadRepository.existsById(id)) {
            leadRepository.deleteById(id);
        } else {
            throw new RuntimeException("Registro nao existe");
        }
    }

    public Lead pesquisarPorCPF (String cpf) {
        Lead lead = leadRepository.findFirstByCpf(cpf);
        if (lead != null) {
            return lead;
        } else {
            throw new RuntimeException("CPF Não cadastrado");
        }
    }

    public List<Lead> pesquisarPeloIdDeProduto(int id) {
        return leadRepository.findByProdutoId(id);
    }
}
