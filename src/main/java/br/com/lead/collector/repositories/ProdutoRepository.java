package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Produto;
import org.springframework.data.repository.CrudRepository;

import javax.sql.rowset.CachedRowSet;

public interface ProdutoRepository extends CrudRepository <Produto, Integer> {
}
