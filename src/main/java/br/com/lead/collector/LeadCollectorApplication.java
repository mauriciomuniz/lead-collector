package br.com.lead.collector;

import br.com.lead.collector.controllers.LeadController;
import br.com.lead.collector.controllers.ProdutoController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeadCollectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeadCollectorApplication.class, args);
	}

	@Autowired
	private LeadController leadController;

	@Autowired
	private ProdutoController produtoController;
}
