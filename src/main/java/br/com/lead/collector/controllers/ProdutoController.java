package br.com.lead.collector.controllers;


import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping("/salvar")
    @ResponseStatus(HttpStatus.CREATED)
    public Produto cadastrarProduto(@RequestBody @Valid Produto produto) {
        Produto objetoProduto = produtoService.salvar(produto);
        return objetoProduto;
    }

    @GetMapping
    public Iterable<Produto> lerTodosOsProdutos() {
        return produtoService.lerTodosOsProdutos();
    }

    @GetMapping("/{id}")
    public Produto pesquisarPorId(@PathVariable(name = "id") int id) {

        try {
            Produto produto = produtoService.buscarProdutoPeloId(id);
            return produto;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@RequestBody @Valid Produto produto, @PathVariable(name = "id") int id) {
        try {
            Produto produtoDB = produtoService.atualizarProduto(id, produto);
            return produtoDB;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deletarProduto(@PathVariable(name="id") int id) {
        try {
            produtoService.deletarProduto(id);
        } catch (RuntimeException e) {
            throw new RuntimeException("Não existe o registro");
        }
    }
}
